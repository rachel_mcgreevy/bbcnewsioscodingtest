//
//  NetworkService.swift
//  fruit
//
//  Created by Rachel McGreevy on 11/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import Foundation

public enum StatsEvent: String {
    case load = "load"
    case display = "display"
    case error = "error"
}

public final class NetworkService {
    
    public static let shared = NetworkService()
    
    let baseUrlString = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master"
    let session = URLSession.shared
    
    // Fetches data and returns array of fruit objects in the completion closure
    func fetchFruitData(completion: @escaping (_ result: [[String: Any]]?) -> Void) {
        
        guard let url = URL(string: baseUrlString + "/data.json") else {
            NetworkService.shared.sendStatsData(forEvent: .error, withData: "couldnt build data fetch url")
            return
        }
        
        // Start timer to time network task
        TimerService.shared.startTimer()
        
        session.dataTask(with: url) { (responseData, response, error) in
        
            TimerService.shared.endTimer(forEvent: .load)
            
            if let data = responseData {
        
                // Parse response into JSON
                guard let JSON = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue:0)) as? [String: Any], let responseJSON = JSON else {
                    NetworkService.shared.sendStatsData(forEvent: .error, withData: "unable to serialise response data into json")
                    return
                }
        
                // Parse JSON into array
                guard let fruitArray = responseJSON["fruit"] as? [[String: Any]] else {
                    NetworkService.shared.sendStatsData(forEvent: .error, withData: "unable to turn response json into arrays")
                    return
                }
        
                // Return array in completion
                completion(fruitArray)
        
            } else if let errorMessage = error?.localizedDescription {
                // responseData was empty and dataTask returned error message
                NetworkService.shared.sendStatsData(forEvent: .error, withData: errorMessage)
            }
            
        }.resume()
    }
    
    // Add event & encoded data parameters to base url & send stats event
    func sendStatsData(forEvent event: StatsEvent, withData data: String) {
        
        let eventString = "event=\(event)"
        let dataString = "data=\(data)"
        let urlString = baseUrlString + "/stats?\(eventString)&\(dataString)"
        
        guard let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: encodedUrlString) else {
            return
        }
        
        session.dataTask(with: url).resume()
    }
    
}
