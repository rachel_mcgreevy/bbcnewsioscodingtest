//
//  FruitViewController.swift
//  fruit
//
//  Created by Rachel McGreevy on 12/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import UIKit

public class FruitViewController: UIViewController {
    
    @IBOutlet var fruitPriceLabel: UILabel?
    @IBOutlet var fruitWeightLabel: UILabel?
    
    public var fruit: Fruit?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        title = fruit?.type
        view.accessibilityIdentifier = "fruitInfoView"
        setupLabels()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TimerService.shared.endTimer(forEvent: .display)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        TimerService.shared.startTimer()
    }
    
    func setupLabels() {
        if let price = fruit?.convertPriceToPounds() {
            fruitPriceLabel?.text = "£\(price)"
        }
        
        if let weight = fruit?.convertWeightToKilos() {
            fruitWeightLabel?.text = "\(weight) kg"
        }
    }
}
