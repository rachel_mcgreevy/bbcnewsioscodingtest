//
//  Fruit.swift
//  fruit
//
//  Created by Rachel McGreevy on 12/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import Foundation

public class Fruit {
    
    var type: String?
    var price: Double?
    var weight: Double?
}

extension Fruit {
    
    // Parse fruit variables from array passed in
    func parseFruitObject(fromArray array: [String: Any]) {
        type = array["type"] as? String
        price = array["price"] as? Double
        weight = array["weight"] as? Double
    }
    
    // Convert current weight value to kilograms, and return with type String
    func convertWeightToKilos() -> String? {
        
        guard let weightInGrams = weight else {
            return nil
        }
        
        let weightInKg = weightInGrams / 1000
        let formattedWeight = String(format: "%.2f", arguments: [weightInKg])
        return formattedWeight
    }
    
    // Convert current price value to pounds & pence, and return with type String
    func convertPriceToPounds() -> String? {
        
        guard let priceInPence = price else {
            return nil
        }
        
        let priceInPounds = priceInPence / 100
        let formattedPrice = String(format: "%.2f", arguments: [priceInPounds])
        return formattedPrice
    }
}
