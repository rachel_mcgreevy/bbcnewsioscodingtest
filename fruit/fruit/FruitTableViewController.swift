//
//  ViewController.swift
//  fruit
//
//  Created by Rachel McGreevy on 11/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import UIKit

class FruitTableViewController: UITableViewController {
    
    var fruitList = [Fruit]()
    open let cellIdentifier = "FruitCell"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        TimerService.shared.startTimer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Fruits"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        refreshControl?.addTarget(self, action: #selector(updateFruitList), for: .valueChanged)
        tableView.accessibilityIdentifier = "fruitListTableView"
        updateFruitList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TimerService.shared.endTimer(forEvent: .display)
    }
    
    @objc func updateFruitList() {
        NetworkService.shared.fetchFruitData() { (results) in
            
            guard let fruits = results else {
                NetworkService.shared.sendStatsData(forEvent: .error, withData: "results from data fetch is nil")
                return
            }
            
            self.fruitList = self.getFruitList(fromArray: fruits)
            
            // Reload tableview on main thread
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
                return
            }
        }
        
        // If function reaches this point, fruit list not updated
        // Need to end refreshing of refresh control anyway
        self.refreshControl?.endRefreshing()
    }
    
    // Turns array of all fruit arrays into 1 array of Fruit objects
    func getFruitList(fromArray array: [[String: Any]]) -> [Fruit] {
        var listOfFruitObjects = [Fruit]()
        for fruit in array {
            let fruitObject = Fruit()
            fruitObject.parseFruitObject(fromArray: fruit)
            listOfFruitObjects.append(fruitObject)
        }
        return listOfFruitObjects
    }
}

// MARK: - UITableView delegate functions

extension FruitTableViewController {
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        // If unable to dequeue cell, create new cell
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        let fruit = fruitList[indexPath.row]
        cell?.textLabel?.text = fruit.type
        
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fruitList.count
    }

    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        TimerService.shared.startTimer()
        
        guard indexPath.row < fruitList.count else {
            NetworkService.shared.sendStatsData(forEvent: .error, withData: "selected row number greater than list count")
            TimerService.shared.resetTimer()
            return
        }
        
        if let fruitVC = storyboard?.instantiateViewController(withIdentifier: "FruitViewController") as? FruitViewController {
            fruitVC.fruit = fruitList[indexPath.row]
            navigationController?.pushViewController(fruitVC, animated: true)
            return
        }
    }
}
