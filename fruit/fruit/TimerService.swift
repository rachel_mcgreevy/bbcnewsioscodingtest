//
//  TimerService.swift
//  fruit
//
//  Created by Rachel McGreevy on 13/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import Foundation

public final class TimerService {
    
    public static let shared = TimerService()

    var startTime: Date?
    var endTime: Date?
    
    // Create start time from current date time
    @objc func startTimer() {
        startTime = Date()
    }
    
    // Create end time from current date time, then log event with duration timer
    func endTimer(forEvent event: StatsEvent) {
        endTime = Date()
        if let eventDuration = TimerService.shared.duration {
            NetworkService.shared.sendStatsData(forEvent: event, withData: eventDuration)
        }
    }
    
    // Reset start and end time values
    func resetTimer() {
        startTime = nil
        endTime = nil
    }
    
    // Calculate timer duration by calculating difference between end and start time, return as string
    var duration: String? {
        guard let end = endTime, let start = startTime else {
            return nil
        }
        let timeIntervalInSeconds = end.timeIntervalSince(start)
        let timeIntervalInMilliseconds = timeIntervalInSeconds * 1000
        return String(format: "%.0f", arguments: [timeIntervalInMilliseconds])
    }
}
