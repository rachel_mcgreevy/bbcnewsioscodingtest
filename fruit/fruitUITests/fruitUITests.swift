//
//  fruitUITests.swift
//  fruitUITests
//
//  Created by Rachel McGreevy on 11/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import XCTest

class fruitUITests: XCTestCase {
    
    var app: XCUIApplication?

    override func setUp() {
        super.setUp()
        
        app = XCUIApplication()
        continueAfterFailure = false
        app?.launch()
    }
    
    override func tearDown() {
        app = nil
        super.tearDown()
    }
    
    func testNavigationBetweenViews() {
        XCTAssert(app?.tables["fruitListTableView"].exists ?? false)
        app?.tables.cells.element(boundBy: 0).tap()
        XCTAssert(app?.otherElements["fruitInfoView"].exists ?? false)
        app?.navigationBars.buttons.element(boundBy: 0).tap()
        XCTAssert(app?.tables["fruitListTableView"].exists ?? false)
    }
    
}
