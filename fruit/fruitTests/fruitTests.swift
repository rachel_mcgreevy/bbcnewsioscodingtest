//
//  fruitTests.swift
//  fruitTests
//
//  Created by Rachel McGreevy on 11/01/2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import XCTest
@testable import fruit

class fruitTests: XCTestCase {
    
    var fruitObject: Fruit?
    
    override func setUp() {
        super.setUp()
        
        fruitObject = Fruit()
        fruitObject?.type = "apple"
        fruitObject?.price = 199
        fruitObject?.weight = 1234
    }
    
    override func tearDown() {
        fruitObject = nil
        super.tearDown()
    }
    
    func testPencePriceConversion() {
        let poundsPrice = fruitObject?.convertPriceToPounds()
        XCTAssert(poundsPrice == "1.99", "Conversion from pence to pounds was incorrect")
    }
    
    func testEmptyPriceConversion() {
        fruitObject?.price = nil
        let poundsPrice = fruitObject?.convertPriceToPounds()
        XCTAssert(poundsPrice == nil, "Conversion function should return nil when price variable is nil")
    }
    
    func testGramsPriceConversion() {
        let kilosWeight = fruitObject?.convertWeightToKilos()
        XCTAssert(kilosWeight == "1.23", "Conversion from grams to kilos was incorrect")
    }
    
    func testEmptyWeightConversion() {
        fruitObject?.weight = nil
        let kilosWeight = fruitObject?.convertWeightToKilos()
        XCTAssert(kilosWeight == nil, "Conversion function should return nil when weight variable is nil")
    }
    
    func testArrayParse() {
        let objectArray = ["type": "banana", "price": Double(99), "weight": Double(200)] as [String : Any]
        fruitObject?.parseFruitObject(fromArray: objectArray)
        XCTAssert(fruitObject?.type == "banana", "Parse function did not parse variable 'type' correctly")
        XCTAssert(fruitObject?.price == 99, "Parse function did not parse variable 'price' correctly")
        XCTAssert(fruitObject?.weight == 200, "Parse function did not parse variable 'weight' correctly")
    }
    
}
